<?php

declare(strict_types=1);

use SimpleSAML\Module\elixir\Disco;
use SimpleSAML\Module\elixir\discowarning\WarningConfiguration;

$config = [
    Disco::DISCO => [
        // required upstream IdP, that will be used as chosen IdP
        Disco::UPSTREAM_IDP_ENTITY_ID => 'https://proxy.aai.lifescience-ri.eu/proxy',
        // warning configuration
        WarningConfiguration::WARNING_CONFIG => [
            // enabled or not
            WarningConfiguration::ENABLED => true,
            // type of the warning - type ERROR prevents users from continuing to log in
            WarningConfiguration::TYPE => WarningConfiguration::WARNING_TYPE_WARNING,
            // displayed title - provide English version of the text
            WarningConfiguration::TITLE => [
                'en' => 'Sample warning title',
            ],
            // displayed text - provide English version of the text
            WarningConfiguration::TEXT => [
                'en' => 'Sample warning text',
            ],
        ],
    ],
];
