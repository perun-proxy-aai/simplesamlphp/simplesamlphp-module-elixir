<?php

declare(strict_types=1);

namespace SimpleSAML\Module\elixir\discowarning;

use SimpleSAML\Configuration;
use SimpleSAML\Module\elixir\Disco;

/**
 * Class WarningConfiguration provides an option to load warning in disco-tpl from different types of sources.
 */
class WarningConfiguration
{
    public const CONFIG_FILE_NAME = 'module_elixir.php';

    public const WARNING_CONFIG = 'warning_config';

    public const TYPE = 'type';

    public const ENABLED = 'enabled';

    public const TITLE = 'title';

    public const TEXT = 'text';

    public const WARNING_TYPE_INFO = 'INFO';

    public const WARNING_TYPE_WARNING = 'WARNING';

    public const WARNING_TYPE_ERROR = 'ERROR';

    public const ALLOWED_TYPES = [self::WARNING_TYPE_INFO, self::WARNING_TYPE_WARNING, self::WARNING_TYPE_ERROR];

    private bool $enabled = false;

    private string $type = '';

    private array $title = [];

    private array $text = [];

    /**
     * Function returns the instance of WarningConfiguration.
     */
    public function __construct()
    {
        $config = Configuration::getConfig(self::CONFIG_FILE_NAME)
            ->getConfigItem(Disco::DISCO)
            ->getConfigItem(self::WARNING_CONFIG, []);
        if (empty($config)) {
            return;
        }
        $this->enabled = $config->getBoolean(self::ENABLED, false);
        $this->type = $config->getValueValidate(self::TYPE, self::ALLOWED_TYPES);
        $this->title = $config->getArray(self::TITLE);
        $this->text = $config->getArray(self::TEXT);
    }

    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getTitle(): array
    {
        return $this->title;
    }

    public function getText(): array
    {
        return $this->text;
    }
}
