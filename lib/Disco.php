<?php

declare(strict_types=1);

namespace SimpleSAML\Module\elixir;

use SimpleSAML\Auth\State;
use SimpleSAML\Configuration;
use SimpleSAML\Logger;
use SimpleSAML\Module\authswitcher\DiscoUtils;
use SimpleSAML\Module\discopower\PowerIdPDisco;
use SimpleSAML\Module\elixir\discowarning\WarningConfiguration;
use SimpleSAML\Utils\HTTP;

/**
 * This class implements a IdP discovery service.
 *
 * This module extends the DiscoPower IdP disco handler, so it needs to be avaliable and enabled and configured.
 *
 * It adds functionality of whitelisting and greylisting IdPs. for security reasons for blacklisting please manipulate
 * directly with metadata. In case of manual idps comment them out or in case of automated metadata fetching configure
 * blacklist in config-metarefresh.php
 */
class Disco extends PowerIdPDisco
{
    private const DEBUG_PREFIX = "elixir:Disco - :";

    public const CONFIG_FILE_NAME = 'module_elixir.php';

    public const URN_CESNET_PROXYIDP_IDPENTITYID = 'urn:cesnet:proxyidp:idpentityid:';

    public const INTERFACE = 'interface';

    public const RPC = 'rpc';

    public const REMOVE_AUTHN_CONTEXT_CLASS_PREFIXES = 'remove_authn_context_class_ref_prefixes';

    public const RETURN = 'return';

    public const AUTH_ID = 'AuthID';

    public const CONTINUE_URL = 'continueUrl';

    // STATE KEYS
    public const SAML_REQUESTED_AUTHN_CONTEXT = 'saml:RequestedAuthnContext';

    public const STATE_AUTHN_CONTEXT_CLASS_REF = 'AuthnContextClassRef';

    public const SAML_SP_SSO = 'saml:sp:sso';

    public const AUTHN_CONTEXT_CLASS_REF = 'AuthnContextClassRef';

    public const NAME = 'name';

    public const WARNING = 'warningAttributes';

    // ROOT CONFIGURATION ENTRY

    public const DISCO = 'disco';

    public const UPSTREAM_IDP_ENTITY_ID = 'upstream_idp_entity_id';

    // VARIABLES

    private array $originalAuthnContextClassRef = [];

    private $discoConfiguration;

    private $upstreamIdpEntityId;

    private $warningConfiguration = null;

    public function __construct(array $metadataSets, $instance)
    {
        parent::__construct($metadataSets, $instance);
        try {
            $elixirConfig = Configuration::getConfig(self::CONFIG_FILE_NAME);
            if (empty($elixirConfig->getValue(self::DISCO, []))) {
                Logger::error(self::DEBUG_PREFIX . "missing key '" . self::DISCO . "' in module config file");
                throw new \Exception();
            }
            $this->discoConfiguration = $elixirConfig->getConfigItem(self::DISCO);
        } catch (\Exception $ex) {
            Logger::error(self::DEBUG_PREFIX . "missing or invalid '" . self::CONFIG_FILE_NAME . "' config file");
            throw $ex;
        }

        if (!array_key_exists(self::RETURN, $_GET)) {
            throw new \Exception('Missing parameter: ' . self::RETURN);
        }
        $returnURL = HTTP::checkURLAllowed($_GET[self::RETURN]);

        parse_str(parse_url($returnURL)['query'], $query);

        if (isset($query[self::AUTH_ID])) {
            $id = explode(':', $query[self::AUTH_ID])[0];
            $state = State::loadState($id, self::SAML_SP_SSO, true);
            if (null !== $state) {
                if (!empty($state[self::SAML_REQUESTED_AUTHN_CONTEXT][self::AUTHN_CONTEXT_CLASS_REF])) {
                    $this->originalAuthnContextClassRef =
                        $state[self::SAML_REQUESTED_AUTHN_CONTEXT][self::AUTHN_CONTEXT_CLASS_REF];
                    $this->removeAuthContextClassRefWithPrefixes($state);
                }
                DiscoUtils::setUpstreamRequestedAuthnContext($state);
                State::saveState($state, self::SAML_SP_SSO);
            }
        }

        $this->upstreamIdpEntityId = $this->discoConfiguration->getString(self::UPSTREAM_IDP_ENTITY_ID, '');
        if (empty($this->upstreamIdpEntityId)) {
            Logger::error(
                self::DEBUG_PREFIX
                . "missing setting of upstream IdP that should be used. Use option '"
                . self::UPSTREAM_IDP_ENTITY_ID
                . "' in the discovery page configuration block to set the EntityID of the IdP"
            );
        }
        if ($this->discoConfiguration->hasValue(WarningConfiguration::WARNING_CONFIG)) {
            $this->warningConfiguration = new WarningConfiguration();
        }
    }

    public function handleRequest()
    {
        $this->start();

        // IF IS SET AUTHN CONTEXT CLASS REF, REDIRECT USER TO THE IDP
        if (!empty($this->originalAuthnContextClassRef)) {
            // Check authnContextClassRef and select IdP directly if the correct value is set
            foreach ($this->originalAuthnContextClassRef as $value) {
                // VERIFY THE PREFIX IS CORRECT AND WE CAN PERFORM THE REDIRECT
                $acrStartSubstr = substr($value, 0, strlen(self::URN_CESNET_PROXYIDP_IDPENTITYID));
                if (self::URN_CESNET_PROXYIDP_IDPENTITYID === $acrStartSubstr) {
                    $idpEntityId = substr($value, strlen(self::URN_CESNET_PROXYIDP_IDPENTITYID), strlen($value));
                    Logger::info('Redirecting to ' . $idpEntityId);
                    $continueUrl = self::buildContinueUrl(
                        $this->spEntityId,
                        $this->returnURL,
                        $this->returnIdParam,
                        $idpEntityId
                    );
                    HTTP::redirectTrustedURL($continueUrl);
                    exit;
                }
            }
        }

        $continueUrl = self::buildContinueUrl(
            $this->spEntityId,
            $this->returnURL,
            $this->returnIdParam,
            $this->upstreamIdpEntityId
        );

        $t = new DiscoTemplate($this->config);
        $t->data[self::WARNING] = $this->warningConfiguration;
        $t->data[self::CONTINUE_URL] = $continueUrl;
        $t->show();
    }

    private static function buildContinueUrl(
        string $entityID,
        string $return,
        string $returnIDParam,
        string $idpEntityId
    ): string {
        return '?' .
            'entityID=' . urlencode($entityID) . '&' .
            'return=' . urlencode($return) . '&' .
            'returnIDParam=' . urlencode($returnIDParam) . '&' .
            'idpentityid=' . urlencode($idpEntityId);
    }

    /**
     * This method remove all AuthnContextClassRef which start with prefix from configuration.
     *
     * @param mixed $state
     */
    public function removeAuthContextClassRefWithPrefixes(&$state)
    {
        $prefixes = $this->discoConfiguration->getArray(self::REMOVE_AUTHN_CONTEXT_CLASS_PREFIXES, []);

        if (empty($prefixes)) {
            return;
        }
        unset($state[self::SAML_REQUESTED_AUTHN_CONTEXT][self::STATE_AUTHN_CONTEXT_CLASS_REF]);
        $filteredAcrs = [];
        foreach ($this->originalAuthnContextClassRef as $acr) {
            $acr = trim($acr);
            $retain = true;
            foreach ($prefixes as $prefix) {
                if (substr($acr, 0, strlen($prefix)) === $prefix) {
                    $retain = false;
                    break;
                }
            }
            if ($retain) {
                $filteredAcrs[] = $acr;
            }
        }
        if (!empty($filteredAcrs)) {
            $state[self::SAML_REQUESTED_AUTHN_CONTEXT][self::STATE_AUTHN_CONTEXT_CLASS_REF] = $filteredAcrs;
        }
    }
}
